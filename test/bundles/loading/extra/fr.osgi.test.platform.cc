/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/dependency/fr.osgi.test.derived.cc
    @brief Class DERIVED::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 15th October 2014 - 15th October 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <ostream>

#include <Activator.hh>
#include <Context.hh>

namespace PLATFORM
{
    class Activator : public OSGi::Activator
    {
    public:
                Activator() = default;
virtual         ~Activator() = default;

    private:

virtual void    doStart(OSGi::Context* pContext) override;
virtual void    doStop(OSGi::Context* pContext) override;
    };


    void Activator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting PLATFORMS::Activator\n";
    }

    void Activator::doStop(OSGi::Context* pContext)
    {
        pContext->out() << "Stopping PLATFORMS::Activator\n";
    }
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(PLATFORM::Activator)
OSGI_END_REGISTER_ACTIVATORS()
