/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/loading/fr.osgi.test.platform.cc
    @brief Class PLATFORM::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 24th February 2013 - 12th July 2015
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Define.hh"

DEFINE_ACTIVATOR(PLATFORM)
