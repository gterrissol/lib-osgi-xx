/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/xp/fr.osgi.test.xp.root2.cc
    @brief Class XPROOT2::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 13th March 2010 - 22nd January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <iostream>

#include <Activator.hh>
#include <Context.hh>
#include <ExtensionPointService.hh>
#include <ServiceRegistry.hh>
#include <tinyxml.h>

namespace XPROOT2
{
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------

    /*!
     */
    class XP : public OSGi::ExtensionPoint
    {
    public:
        XP() = default;                                                                                     //!< 
virtual ~XP() = default;                                                                                    //!< 

    private:

virtual void    doHandleExtension(OSGi::Bundle::CPtr pBundle, const OSGi::XmlElement* pExtension) override; //!< 
virtual void    doRemoveExtension(OSGi::Bundle::CPtr pBundle, const OSGi::XmlElement* pExtension) override; //!< 

    };


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*!
     */
    void XP::doHandleExtension(OSGi::Bundle::CPtr pBundle, const OSGi::XmlElement* pExtension)
    {
        std::cout << "XP::doHandleExtension(" << pBundle->name() << ", " << *pExtension << ")" << std::endl;
    }


    /*!
     */
    void XP::doRemoveExtension(OSGi::Bundle::CPtr pBundle, const OSGi::XmlElement* pExtension)
    {
        std::cout << "XP::doRemoveExtension(" << pBundle->name() << ", " << *pExtension << ")" << std::endl;
    }



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! @brief Bundle fr.osgi.test.root2 activator.
        @version 
     */
    class Activator : public OSGi::Activator
    {
    public:
        //! @name
        //@{
                Activator() = default;                          //!< Constructor.
virtual         ~Activator() = default;                         //!< Destructor.
        //@}

    private:
        //! @name
        //@{
virtual void    doStart(OSGi::Context* pContext) override;  //!< Launching.
virtual void    doStop(OSGi::Context* pContext) override;   //!< Halting.
        //@}
    };


    /*!
     */
    void Activator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting XPROOT2::Activator" << std::endl;
        auto    lService = pContext->services()->findByTypeAndName<OSGi::ExtensionPointService>("osgi.core.xp");

        auto    lXP2 = std::make_shared<XP>();
        auto    lB   = pContext->findBundle("fr.osgi.test.xp.root2");

        lService->checkIn(lB, "xp_2nd", lXP2);
    }


    /*!
     */
    void Activator::doStop(OSGi::Context* pContext)
    {
        auto    lService = pContext->services()->findByTypeAndName<OSGi::ExtensionPointService>("osgi.core.xp");
        lService->checkOut("xp_2nd");

        pContext->out() << "Stopping XPROOT2::Activator" << std::endl;
    }
}


OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(XPROOT2::Activator)
OSGI_END_REGISTER_ACTIVATORS()
