/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/dependency/fr.osgi.test.derived.cc
    @brief Class DERIVED::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 13th June 2013 - 20th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <ostream>

#include <Activator.hh>
#include <Context.hh>

#include "fr.osgi.test.base.hh"

namespace DERIVED
{
    class D : public BASE::C
    {
    public:

                D(OSGi::Context* pContext);
virtual         ~D();

virtual void    foo() const override;
    };

    D::D(OSGi::Context* pContext)
        : BASE::C(pContext)
    {
        context()->out() << "D::D()\n";
    }

    D::~D()
    {
        context()->out() << "D::~D()\n";
    }

    void D::foo() const
    {
        context()->out() << "D::foo()\n";
    }


    class Activator : public OSGi::Activator
    {
    public:
                Activator() = default;
virtual         ~Activator() = default;

    private:

virtual void    doStart(OSGi::Context* pContext) override;
virtual void    doStop(OSGi::Context* pContext) override;
    };


    void Activator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting DERIVED::Activator\n";
        new D{pContext};
    }

    void Activator::doStop(OSGi::Context* pContext)
    {
        pContext->out() << "Stopping DERIVED::Activator\n";
    }
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(DERIVED::Activator)
OSGI_END_REGISTER_ACTIVATORS()
