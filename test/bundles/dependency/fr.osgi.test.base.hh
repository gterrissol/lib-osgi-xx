/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef FR_OSGI_TEST_BASE_HH
#define FR_OSGI_TEST_BASE_HH

#include "OSGi.hh"

/*! @file bundles/dependency/fr.osgi.test.base.hh
    @brief Class BASE::C declaration.
    @author @ref Guillaume_Terrissol
    @date 13th June 2013 - 13th June 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Context.hh"

namespace BASE
{
    class OSGI_EXPORT C
    {
    public:
                            C(OSGi::Context* pContext);
virtual                     ~C();

virtual void                foo() const;

    protected:

        OSGi::Context::Ptr  context() const;

    private:

        std::weak_ptr<OSGi::Context>    mContext;
    };
}

#endif  // FR_OSGI_TEST_BASE_HH
