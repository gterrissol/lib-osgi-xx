/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/dependency/fr.osgi.test.base.cc
    @brief Classes BASE::C & BASE::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 13th June 2013 - 20th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <ostream>
#include <set>

#include <Activator.hh>
#include <Context.hh>

#include "fr.osgi.test.base.hh"


namespace BASE
{
    std::vector<std::unique_ptr<C>> gMyCs;

    C::C(OSGi::Context* pContext)
        : mContext{pContext->shared_from_this()}
    {
        context()->out() << "C::C()\n";
        gMyCs.push_back(std::unique_ptr<C>{this});
    }

    C::~C()
    {
        context()->out() << "C::~C()\n";
    }

    OSGi::Context::Ptr C::context() const
    {
        return mContext.lock();
    }

    void C::foo() const
    {
        context()->out() << "C::foo()\n";
    }

    class Activator : public OSGi::Activator
    {
    public:
                Activator() = default;
virtual         ~Activator() = default;
    private:
virtual void    doStart(OSGi::Context* pContext) override;
virtual void    doStop(OSGi::Context* pContext) override;
    };

    void Activator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting BASE::Activator\n";
        new C{pContext};
    }

    void Activator::doStop(OSGi::Context* pContext)
    {
        pContext->out() << "Stopping BASE::Activator\n";
        while(!gMyCs.empty()) gMyCs.pop_back();
    }
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(BASE::Activator)
OSGI_END_REGISTER_ACTIVATORS()
