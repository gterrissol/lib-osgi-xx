/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file bundles/loading/fr.osgi.test.1st.cc
    @brief Class N1ST::Activator definition.
    @author @ref Guillaume_Terrissol
    @date 27th March 2014 - 12th July 2015
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <ostream>

#include <OSGi/Activator.hh>
#include <OSGi/Context.hh>

namespace N
{
    class Activator : public OSGi::Activator
    {
    public:
                Activator() = default;
virtual         ~Activator() = default;
    private:
        void    doStart(OSGi::Context* pContext) override;
        void    doStop(OSGi::Context* pContext) override;
    };

    void Activator::doStart(OSGi::Context* pContext)
    {
        pContext->out() << "Starting N::Activator" << std::endl;
    }

    void Activator::doStop(OSGi::Context* pContext)
    {
        pContext->out() << "Stopping N::Activator" << std::endl;
    }
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(N::Activator)
OSGI_END_REGISTER_ACTIVATORS()
