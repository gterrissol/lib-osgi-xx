<?xml version="1.0" encoding='ISO-8859-1' standalone='yes' ?>
<xsl:stylesheet	version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

		<xsl:template match="/">
		<HTML>
			<HEAD>
				<TITLE>Test report - Version <xsl:value-of select="/TestRun/Context/@Version"/></TITLE>
				<STYLE>
					TABLE				{ color:#404040; font-family:'sanserif'; }
					TH					{ color:#FFFFFF; font-family:'sanserif'; font-weight:bold; background-color:#0000A0; }
					H1					{ color:#404080; font-family:'sanserif'; border-style:solid; border-width:0px; border-bottom-width:3px; border-bottom-color:#404080; }
					H2					{ color:#202080; font-family:'sanserif'; border-style:solid; border-width:0px; border-bottom-width:2px; border-bottom-color:#202080; }
					SPAN.good			{ color:#00D000; font-weight:bold; }
					SPAN.critical		{ color:#D00000; font-weight:bold; }
				</STYLE>
			</HEAD>
			<BODY>
				<H1>Test report <xsl:value-of select="/TestRun/Context/@Version"/> - OSGi <xsl:value-of select="/TestRun/Context/@OSGi"/></H1>
				<xsl:apply-templates select="/TestRun/Context"/>
				<xsl:apply-templates select="/TestRun/Statistics"/>
				<xsl:apply-templates select="/TestRun/FailedTests"/>
			</BODY>
		</HTML>
	</xsl:template>

	<xsl:template match="Context">
		<H2>Context</H2>
		<TABLE>
			<TR>
				<TD>Platform</TD><TD> : </TD>
				<TD><xsl:value-of select="/TestRun/Context/@System"/></TD>
				<TD><xsl:value-of select="/TestRun/Context/@Release"/></TD>
			</TR>
			<TR>
				<TD>GCC</TD><TD> : </TD>
				<TD><xsl:value-of select="/TestRun/Context/@Compiler"/></TD>
			</TR>
			<TR>
				<TD>Date </TD><TD> : </TD>
				<!--TD><xsl:value-of select="/TestRun/Context/@Date"/></TD-->
				<TD><xsl:value-of select="concat(substring(/TestRun/Context/@Date,9,2),'/',substring(/TestRun/Context/@Date,6,2),'/',substring(/TestRun/Context/@Date,1,4))"/></TD>
				<TD><xsl:value-of select="substring(/TestRun/Context/@Date,12,8)"/></TD>
				
			</TR>
		</TABLE>
	</xsl:template>

	<xsl:template match="FailedTests">
		<H2>Failed tests</H2>
		<xsl:choose>
		<xsl:when test="FailedTest">
			<TABLE>
				<TR>
					<TH>Name</TH>
					<TH>Error type</TH>
					<TH>Location</TH>
					<TH>Message</TH>
				</TR>
				<xsl:apply-templates select="FailedTest"/>
			</TABLE>
		</xsl:when>
		<xsl:otherwise>
			<SPAN class="good">No test failed.</SPAN>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="FailedTest">
		<TR>
			<TD valign='top'><xsl:apply-templates select="Name"/></TD>
			<TD valign='top'><xsl:apply-templates select="FailureType"/></TD>
			<TD valign='top'><xsl:apply-templates select="Location"/></TD>
			<TD valign='top'><xsl:apply-templates select="Message"/></TD>
		</TR>
	</xsl:template>

	<xsl:template match="Name|FailureType|Message"><xsl:value-of select="."/></xsl:template>

	<xsl:template match="Location">
		<xsl:if test=".">
			<xsl:value-of select="File"/> - line <xsl:value-of select="Line"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="SuccessfulTests">
		<H2>Succcessful tests</H2>
		<xsl:choose>
		<xsl:when test="Test">
		<TABLE>
			<TR>
				<TH>Name</TH>
			</TR>
			<xsl:apply-templates select="Test"/>
		</TABLE>
		</xsl:when>
		<xsl:otherwise>
			<SPAN class="critical">No test succeeded.</SPAN>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="Test">
		<TR>
			<TD><xsl:apply-templates select="Name"/></TD>
		</TR>
	</xsl:template>

	<xsl:template match="Statistics">
		<H2>Statistics</H2>
		<TABLE>
			<TR>
				<TH>Status</TH>
				<TH>Count</TH>
			</TR>
			<TR>
				<TD>Run tests</TD>
				<TD align="right"><xsl:value-of select="Tests"/></TD>
			</TR>
			<TR>
				<TD>Failed tests</TD>
				<TD align="right"><xsl:value-of select="FailuresTotal"/></TD>
			</TR>
			<TR>
				<TD>Errors</TD>
				<TD align="right"><xsl:value-of select="Errors"/></TD>
			</TR>
			<TR>
				<TD>Failures</TD>
				<TD align="right"><xsl:value-of select="Failures"/></TD>
			</TR>
		</TABLE>
	</xsl:template>

</xsl:stylesheet>
