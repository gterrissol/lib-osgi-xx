/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef TEST_DEFINE_HH
#define TEST_DEFINE_HH

/*! @file test/Define.hh
    @brief Helper macros.
    @author @ref Guillaume_Terrissol
    @date 25th May 2013 - 20th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <ostream>

#include <Activator.hh>
#include <Context.hh>

//! Macro defining of a minimal activator (used for test bundles).
#define DEFINE_ACTIVATOR(pName)                                         \
namespace pName                                                         \
{                                                                       \
    class Activator : public OSGi::Activator                            \
    {                                                                   \
    public:                                                             \
                Activator() = default;                                  \
virtual         ~Activator() = default;                                 \
    private:                                                            \
virtual void    doStart(OSGi::Context* pContext) override;              \
virtual void    doStop(OSGi::Context* pContext) override;               \
    };                                                                  \
                                                                        \
    void Activator::doStart(OSGi::Context* pContext)                    \
    {                                                                   \
        pContext->out() << "Starting "#pName"::Activator" << std::endl; \
    }                                                                   \
                                                                        \
    void Activator::doStop(OSGi::Context* pContext)                     \
    {                                                                   \
        pContext->out() << "Stopping "#pName"::Activator" << std::endl; \
    }                                                                   \
}                                                                       \
                                                                        \
OSGI_REGISTER_ACTIVATORS()                                              \
    OSGI_DECLARE_ACTIVATOR(pName::Activator)                            \
OSGI_END_REGISTER_ACTIVATORS()


#endif  // De BIN_UTILS_HH
