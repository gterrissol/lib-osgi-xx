cmake_minimum_required(VERSION 2.8.8)

include_directories(@CONF_INCLUDE_DIR@)

include(CMakeParseArguments)

file(MAKE_DIRECTORY ${PROJECT_BINARY_DIR}/bin/bundles)

# add_bundle(<name>             # Bundle name (e.g. fr.osgi.deployment).
#   VERSION      version        # Bundle version (3 numbers, dot-separated, e.g. 1.2.10).
#   SOURCES      files...       # Bundle source files.
#   [DEPENDS     depends...]    # Optional list of dependencies.
#   [ARCHIVES    flags...]      # Optional list of dependencies (static libraries).
#   [INPUT_PATHS paths...]      # Optional input paths for the files to pack into the bundle.
#   [EXCLUDE_FROM_ALL]          # Option to remove the bundle from target "all".
# )
#
# <name>.properties and <name>.extensions.xml, if found, are automatically handled.
#
# The bundle final target is named in the variable ${<name>_bndl}.
#
function(add_bundle BUNDLE_NAME)

    set(Arguments VERSION)
    set(Options EXCLUDE_FROM_ALL)
    set(MultiArguments ARCHIVES DEPENDS INPUT_PATHS SOURCES)
    cmake_parse_arguments(BUNDLE "${Options}" "${Arguments}" "${MultiArguments}" ${ARGN})

    set(LIBRARY     ${BUNDLE_NAME})
    set(BUNDLE      ${BUNDLE_NAME}_${BUNDLE_VERSION}.bndl)
    set(BUNDLESPEC  ${BUNDLE_NAME}.bundlespec)

    if(NOT TARGET ${LIBRARY})
        add_library(${LIBRARY} MODULE EXCLUDE_FROM_ALL ${BUNDLE_SOURCES})
        set_target_properties(${LIBRARY} PROPERTIES PREFIX "")

        target_compile_definitions(${LIBRARY} PUBLIC "-DBUNDLE_NAME=\"${BUNDLE_NAME}\"")
        if(${CMAKE_COMPILER_IS_GNUCXX})
            target_link_libraries(${LIBRARY} -Wl,--whole-archive ${BUNDLE_ARCHIVES} -Wl,--no-whole-archive)
        endif()
        target_link_libraries(${LIBRARY} OSGi++)
        if(NOT "${BUNDLE_DEPENDS}" STREQUAL "")
            add_dependencies(${LIBRARY} ${BUNDLE_DEPENDS})
        endif()
    endif()

    # Automatically adds properties file, if found.
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLE_NAME}.properties)
        set(OTHER_FILES -f${BUNDLE_NAME}.properties=${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLE_NAME}.properties)
    endif()
    # Automatically adds extension file, if found.
    if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLE_NAME}.extensions.xml)
        set(OTHER_FILES ${OTHER_FILES} -fextensions.xml=${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLE_NAME}.extensions.xml)
    endif()

    configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLESPEC}.in ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLESPEC})

    # Inputs.
    set(INPUT_PATHS)    # NB: Uses configured files first.
    foreach(path ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR} ${BUNDLE_INPUT_PATHS})
        set(INPUT_PATHS ${INPUT_PATHS} -I${path})
    endforeach()

    add_custom_command(OUTPUT ${PROJECT_BINARY_DIR}/bin/bundles/${BUNDLE}
        COMMENT Packaging ${BUNDLE}
        COMMAND ${OSGi_PACKAGER}
                ${INPUT_PATHS}
                -o ${PROJECT_BINARY_DIR}/bin/bundles/${BUNDLE}
                ${OTHER_FILES}
                ${CMAKE_CURRENT_BINARY_DIR}/${BUNDLESPEC}   # Bundlespec file is the last argument.
        DEPENDS ${LIBRARY}
                ${CMAKE_CURRENT_SOURCE_DIR}/${BUNDLESPEC}.in
    )

    # Main target : bundles.
    if(NOT TARGET bundles)
        add_custom_target(bundles)
    endif()

    if(${BUNDLE_EXCLUDE_FROM_ALL})
        add_custom_target(${BUNDLE}    DEPENDS ${PROJECT_BINARY_DIR}/bin/bundles/${BUNDLE})
    else()
        add_custom_target(${BUNDLE} ALL DEPENDS ${PROJECT_BINARY_DIR}/bin/bundles/${BUNDLE})
        add_dependencies(bundles ${BUNDLE})
    endif()

    set(${BUNDLE_NAME}_bndl ${BUNDLE} PARENT_SCOPE)

endfunction()
