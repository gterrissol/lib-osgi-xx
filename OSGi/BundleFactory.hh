/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_BUNDLEFACTORY_HH
#define OSGI_BUNDLEFACTORY_HH

#include "OSGi.hh"

/*! @file OSGi/BundleFactory.hh
    @brief Class OSGi::BundleFactory header.
    @author @ref Guillaume_Terrissol
    @date 9th February 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <string>

#include "Bundle.hh"
#include "LifeCycle.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                                Bundle Factory
//------------------------------------------------------------------------------

    /*! @brief Bundle factory.
        @version 0.7
    */
    class OSGI_EXPORT BundleFactory
    {
    public:
        //! @name Pointers
        //@{
        using Ptr   = std::unique_ptr<BundleFactory>;                   //!< Pointer to factory.
        //@}
        //! @name Constructor & destructor
        //@{
                        BundleFactory();                                //!< Constructor.
virtual                 ~BundleFactory();                               //!< Destructor.
        //@}
        //! @name "Factory"
        //@{
        Bundle::Ptr     makeBundle(std::string pFilename,
                                   int         pId,
                                   const Context* pContext) const;      //!< Bundle creation.
        LifeCycle::Ptr  makeBundleLifeCycle() const;                    //!< Which life cycle.
        //@}

    protected:

virtual void            setUp(LifeCycle* pLifeCycle) const;             //!< Life cycle definition.


    private:

virtual Bundle::Ptr     doMakeBundle(std::string    pFilename,
                                     int            pId,
                                     const Context* pContext) const;    //!< Bundle creation.
    };
}

#endif  // OSGI_BUNDLEFACTORY_HH
