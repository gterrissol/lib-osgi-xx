/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_UTILS_HH
#define OSGI_UTILS_HH

#include "OSGi.hh"

/*! @file OSGi/Utils.hh
    @brief Useful functions declaration.
    @author @ref Guillaume_Terrissol
    @date 27th December 2009 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <string>
#include <vector>

namespace OSGi
{
//------------------------------------------------------------------------------
//                               Utility Functions
//------------------------------------------------------------------------------

    //! @name Operations on strings
    //@{
    std::string&                trimString(std::string& pText);                     //!< String trimming.
    std::string                 trimString(std::string&& pText);                    //!< @copybrief trimString(std::string&)
    std::vector<std::string>    splitString(const std::string& pText, char pSep);   //!< String splitting.
    //@}
    //! @name Operations on paths
    //@{
    bool                        isPathAbsolute(const std::string& pPath);           //!< Absolute path ?
    std::string                 workingDirectory();                                 //!< [Absolute] working directory.
    std::string                 fromWorkingDirectory(const std::string& pPath);     //!< Path relative to working directory.
    //@}
    //! @name Operations on directories
    //@{
    void                        removeDirectory(const std::string& pDirName);       //!< Directory removal.
    void                        emptyDirectory(const std::string& pDirName);        //!< Directory contents removal.
    //@}
}

#endif  // OSGI_UTILS_HH
