/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_EXTENSIONPOINT_HH
#define OSGI_EXTENSIONPOINT_HH

#include "OSGi.hh"

/*! @file OSGi/ExtensionPoint.hh
    @brief Class OSGi::ExtensionPoint header.
    @author @ref Guillaume_Terrissol
    @date 9th January 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Bundle.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                          Extension Point Base Class
//------------------------------------------------------------------------------

    /*! @brief Base class for extension points.
        @version 0.7
    */
    class OSGI_EXPORT ExtensionPoint
    {
    public:
        //! @name Pointers
        //@{
        using Ptr = std::shared_ptr<ExtensionPoint>;                                        //!< Pointer to ExtensionPoint.
        //! @name Constructor & destructor
        //@{
                ExtensionPoint();                                                           //!< Default constructor.
virtual         ~ExtensionPoint();                                                          //!< Destructor.
        //@}
        //! @name Interface
        //@{
        void    handleExtension(Bundle::CPtr pBundle, const XmlElement* pExtension);        //!< Extension handling.
        void    removeExtension(Bundle::CPtr pBundle, const XmlElement* pExtension);        //!< Extension removing.
        //@}

    private:
        //! @name Implementation
        //@{
virtual void    doHandleExtension(Bundle::CPtr pBundle, const XmlElement* pExtension) = 0;  //!< Extension handling.
virtual void    doRemoveExtension(Bundle::CPtr pBundle, const XmlElement* pExtension);      //!< Extension removing.
        //@}
    };
}

#endif  // OSGI_EXTENSIONPOINT_HH
