/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_SERVICE_HH
#define OSGI_SERVICE_HH

#include "OSGi.hh"

/*! @file OSGi/Service.hh
    @brief Class OSGi::Service header.
    @author @ref Guillaume_Terrissol
    @date 12th December 2009 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <memory>
#include <string>

#include "Signal.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                              Service Base Class
//------------------------------------------------------------------------------

    /*! @brief Service base class.
        @version 0.8

        A service is a named component supplied by a bundle, providing various
        features to an application (e.g. logging, configuration, networking...).
        @n
        A @ref OSGi_Default_Services_Page "few services" are available in the
        core components.
        @sa ServiceRegistry
     */
    class OSGI_EXPORT Service : public std::enable_shared_from_this<Service>
    {
    public:
        //! @name Pointers
        //@{
        using Ptr = std::shared_ptr<Service>;                       //!< Pointer to service.
        //@}
        //! @name Constructor & destructor
        //@{
                    Service();                                      //!< Default constructor.
virtual             ~Service();                                     //!< Destructor.
        //@}
        //! @name Interface
        //@{
 static std::string staticName();                                   //!< This type (Service) name.
        std::string dynamicName() const;                            //!< An instance [real] type name.
        bool        isA(const std::string& pTypeName) const;        //!< Is a ... ?
        //@}
        //! @name Implementation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const = 0; //!< Is type ... ?


    private:

virtual std::string typeName() const = 0;                           //!< [Real] type name.
        //@}
    };
}

#endif  // OSGI_SERVICE_HH
