/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_EXTENSIONPOINTSERVICE_HH
#define OSGI_EXTENSIONPOINTSERVICE_HH

#include "OSGi.hh"

/*! @file OSGi/ExtensionPointService.hh
    @brief Class OSGi::ExtensionPointService header.
    @author @ref Guillaume_Terrissol
    @date 9th January 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "BundleMgr.hh"
#include "ExtensionPoint.hh"
#include "Service.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                               Extension Points
//------------------------------------------------------------------------------

    /*! @brief ExtensionPoint Service.
        @version 0.9
     */
    class OSGI_EXPORT ExtensionPointService : public Service
    {
    public:
        //! @name Pointers
        //@{
        using Ptr = std::shared_ptr<ExtensionPointService>;                 //!< Pointer to service.
        //@}
        //! @name Constructor & destructor
        //@{
                    ExtensionPointService();                                //!< Default constructor.
virtual             ~ExtensionPointService();                               //!< Destructor.
        //@}
        //! @name Interface
        //@{
 static std::string staticName();                                           //!< This type (ExtensionPointService) name.

        void        checkIn(Bundle::CPtr        pBundle,
                            const std::string&  pId,
                            ExtensionPoint::Ptr pXP);                       //!< Extension point registering.
        void        checkOut(const std::string& pId);                       //!< Extension point unregistering. 
        //@}
        //! @name Slots
        //@{
        void        onBundleStarted(Bundle::Ptr pStarted);                  //!< Called on bundle start.
        void        onBundleStopped(Bundle::Ptr pStopped);                  //!< Called on bundle stop.
        //@}
        //! @name Implementation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< @copybrief Service::isType()


    private:

virtual std::string typeName() const override;                              //!< @copybrief Service::typeName()
        //@}
        OSGI_PIMPL()
    };
}

#endif  // OSGI_EXTENSIONPOINTSERVICE_HH
