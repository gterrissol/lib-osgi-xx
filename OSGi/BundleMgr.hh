/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_BUNDLEMGR_HH
#define OSGI_BUNDLEMGR_HH

#include "OSGi.hh"

/*! @file OSGi/BundleMgr.hh
    @brief Class OSGi::BundleMgr header.
    @author @ref Guillaume_Terrissol
    @date 17th December 2009 - 10th April 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <memory>
#include <vector>

#include "Context.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                                Bundle Manager
//------------------------------------------------------------------------------

    /*! @brief Bundle manager.
        @version 0.6

        The bundle manager is the main object in OSGi.@n
        Several managers may exist at the same time in an application.
        @sa @ref OSGi_BundleMgr_Args_Page
     */
    class OSGI_EXPORT BundleMgr
    {
        OSGI_PRIVATE_CONSTRUCTOR()
    public:
        //! @name Types
        //@{
        using Ptr  = std::unique_ptr<BundleMgr>;                        //!< Pointer to bundle manager.
        using Args = Context::Args;                                     //!< Argument list.
        //@}
        //! @name Actions
        //@{
        void                    loadBundles();                          //!< Bundles loading.
        void                    startBundles();                         //!< Bundles resolving and launching.
        void                    finalize();                             //!< Bundles stopping.
        //@}
        //! @name Components
        //@{
        Context*                context();                              //!< Environment context.
        ServiceRegistry*        services();                             //!< Service list.
        Properties*             properties();                           //!< General properties.
        //@}
        //! @name Constructors & destructor
        //@{
 static Ptr                     make(const Args& pArgs);                //!< Maker.
 static Ptr                     make(const Args& pArgs,
                                     BundleFactory::Ptr pFactory);      //!< Specialized maker.
                                BundleMgr(const Args&        pArgs,
                                          BundleFactory::Ptr pFactory,
                                          const Key&);                  //!< Constructor.
                                ~BundleMgr();                           //!< Destructor.
        //@}
    private:

        OSGI_PIMPL()
    };
}

#endif  // OSGI_BUNDLEMGR_HH
