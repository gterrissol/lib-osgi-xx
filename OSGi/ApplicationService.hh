/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_APPLICATIONSERVICE_HH
#define OSGI_APPLICATIONSERVICE_HH

#include "OSGi.hh"

/*! @file OSGi/ApplicationService.hh
    @brief Class OSGi::ApplicationService header.
    @author @ref Guillaume_Terrissol
    @date 1st September 2011 - 30th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Context.hh"
#include "Service.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                              Application Service
//------------------------------------------------------------------------------

    /*! @brief Application Service.
        @version 0.8

        This service provides a C/C++ application main entry point.@n
        Subclasses shall implement the process(const Args&, Context*) method.
     */
    class OSGI_EXPORT ApplicationService : public Service
    {
    public:
        //! @name Types
        //@{
        using   Ptr     = std::shared_ptr<ApplicationService>;              //!< Pointer to service.
        using   Args    = std::vector<std::string>;                         //!< Argument list.
        //@}
        //! @name Constructor & destructor
        //@{
                    ApplicationService();                                   //!< Constructor.
virtual             ~ApplicationService();                                  //!< Destructor.
        //@}
        //! @name Interface
        //@{
 static std::string staticName();                                           //!< This type (ApplicationService) name.

        int         main(int pArgc, char* pArgv[], Context* pContext);      //!< Application main function (C style).
        int         main(const Args& pArgs, Context* pContext);             //!< Application main function (C++ style).
        //@}
        //! @name Implementation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< @copybrief Service::isType()


    private:

virtual std::string typeName() const override;                              //!< @copybrief Service::typeName()
virtual int         process(const Args& pArgs, Context* pContext) = 0;      //!< Application main function.
        //@}
    };
}

#endif  // OSGI_APPLICATIONSERVICE_HH
