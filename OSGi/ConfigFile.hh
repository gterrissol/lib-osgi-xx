/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_CONFIGFILE_HH
#define OSGI_CONFIGFILE_HH

#include "OSGi.hh"

/*! @file OSGi/ConfigFile.hh
    @brief Class OSGi::ConfigFile header.
    @author @ref Guillaume_Terrissol
    @date 12th December 2009 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <string>

namespace OSGi
{
//------------------------------------------------------------------------------
//                                Ini File Reader
//------------------------------------------------------------------------------

    /*! @brief Configuration file reader.
        @version 0.8

        This class my be used to parse a standard @b .ini file, and fill a
        Properties instance with read pieces of information.
     */
    class OSGI_EXPORT ConfigFile
    {
    public:

                ConfigFile(std::string pFilename);                          //!< Constructor.
        //! @name Properties update
        //@{
        void    update(Properties& pProps);                                 //!< Standard update.
        void    update(Properties& pProps, std::string pDefaultSection);    //!< Update with default section.
        //@}

    private:

        std::string mFilename;                                              //!< Configuration filename.
    };
}

#endif  // OSGI_CONFIGFILE_HH
