/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

/*! @file OSGi/ServiceRegistry.inl
    @brief Class OSGi::ServiceRegistry inline methods.
    @author @ref Guillaume_Terrissol
    @date 28th August 2011 - 20th August 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
//------------------------------------------------------------------------------
//                     Service Registry : Service Management
//------------------------------------------------------------------------------

    /*! @tparam TS    Type of the service to find
        @param  pName Name of the service to find
        @return The service, of type @p TS , named @p pName, if defined, a null pointer otherwise
     */
    template<class TS>
    inline TS* ServiceRegistry::findByTypeAndName(const std::string& pName)
    {
        return dynamic_cast<TS*>(findByName(pName));
    }
}
