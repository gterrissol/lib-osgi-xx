/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_EXCEPTION_HH
#define OSGI_EXCEPTION_HH

#include "OSGi.hh"

/*! @file OSGi/Exception.hh
    @brief Class OSGi::Exception header.
    @author @ref Guillaume_Terrissol
    @date 6th March 2010 - 15th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <exception>
#include <string>

namespace OSGi
{
//------------------------------------------------------------------------------
//                                  Exceptions
//------------------------------------------------------------------------------

    /*! @brief Exception base class.
        @version 0.7

        This is the base class from inherited by every exception class in the
        @ref OSGi library.
    */
    class OSGI_EXPORT Exception : public std::exception
    {
    public:
        //! @name Interface
        //@{
explicit            Exception(std::string pMsg);    //!< Constructor.
virtual             ~Exception() noexcept;          //!< Destructor.
virtual const char* what() const noexcept override; //!< What happened ?
        //@}

    private:

        std::string mMessage;                       //!< Error message.
    };


    /*! @brief Input/output error.
        @version 0.5

        This exception class is used to signal errors related to input/output
        operations (file handling, logs, etc).
     */
    class OSGI_EXPORT IOError : public Exception
    {
    public:
        //! @name Interface
        //@{
explicit    IOError(std::string pMsg);  //!< Constructor.
virtual     ~IOError() noexcept;        //!< Destructor.
        //@}
    };


    /*! @brief Behavior error.
        @version 0.5

        This exception class is used like an assertion, to check preconditions
        mainly.
     */
    class OSGI_EXPORT LogicError : public Exception
    {
    public:
        //! @name Interface
        //@{
explicit    LogicError(std::string pMsg);   //!< Constructor.
virtual     ~LogicError() noexcept;         //!< Destructor.
        //@}
    };


    /*! @brief Bundle management error.
        @version 0.5

        This exception class is used to signal errors related to bundle misuse
        or dysfunction.
     */
    class OSGI_EXPORT BundleError : public Exception
    {
    public:
        //! @name Interface
        //@{
explicit    BundleError(std::string pMsg);  //!< Constructor.
virtual     ~BundleError() noexcept;        //!< Destructor.
        //@}
    };
}

#endif  // OSGI_EXCEPTION_HH
