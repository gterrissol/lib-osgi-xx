/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_PLATFORM_HH
#define OSGI_PLATFORM_HH

#include "OSGi.hh"

/*! @file OSGi/Platform.hh
    @brief Class OSGi::Platform header.
    @author @ref Guillaume_Terrissol
    @date 17th December 2009 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <string>

namespace OSGi
{
//------------------------------------------------------------------------------
//                             Platform Information
//------------------------------------------------------------------------------

    /*! @brief System information.
        @version 0.9

        This class allows to retrieve a few pieces of information about the
        system this library is used on.
     */
    class OSGI_EXPORT Platform
    {
    public:
        //! @name Information
        //@{
 static std::string osName();           //!< Operating system name.
 static std::string osLongerName();     //!< Operating system more specific name.
 static std::string osVersion();        //!< Operating system version.
 static std::string osArchitecture();   //!< Hardware (processor(s)).
 static std::string hostName();         //!< Host name.
        //@}
    };
}

#endif  // OSGI_PLATFORM_HH
