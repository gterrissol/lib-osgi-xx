/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_OSGI_HH
#define OSGI_OSGI_HH

/*! @file OSGi/OSGi.hh
    @brief Forward declarations for module @ref OSGi.
    @author @ref Guillaume_Terrissol
    @date 12th December 2009 - 11th April 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

//! Export macro (a-la windows)
#define OSGI_EXPORT __attribute__((visibility("default"))) 

//! Prevent class instantiation declaring a protected parameter (add to the constructor(s) to guard).
#define OSGI_PROTECTED_CONSTRUCTOR() \
    protected: \
        struct Key { };
//! Prevent class instantiation declaring a private parameter (add to the constructor(s) to guard).
#define OSGI_PRIVATE_CONSTRUCTOR() \
    private: \
        struct Key { };

#ifndef NOT_FOR_DOXYGEN
#   define TIXML_USE_STL
class TiXmlElement;
#endif  // NOT_FOR_DOXYGEN

namespace OSGi
{
//------------------------------------------------------------------------------
//                       OSGi Classes Forward Declaration
//------------------------------------------------------------------------------

    class Activator;
    class Bundle;
    class BundleFactory;
    class BundleInstallerService;
    class BundleMgr;
    class Context;
    class Exception;
    class ExtensionPoint;
    class ExtensionPointService;
    class LifeCycle;
    class Platform;
    class Properties;
    class Service;
    class ServiceRegistry;
    using XmlElement = TiXmlElement;    //!< Alias for TiXmlElement (for consistency).

    template<class TC>
    class CGuard;
    template<class TPrivate>
    class PImpl;
    /// @cond DEVELOPMENT
    template<class... TArgs>
    class Signal;
    /// @endcond


    /*! @namespace OSGi
        @brief OSGi light implementation (C++).
        @version 0.9.38
     */

    /*! @page team_page Team
        @section Guillaume_Terrissol Guillaume Terrissol
        <b>Main programmer</b>@n
    */
}

#endif  // OSGI_OSGI_HH
