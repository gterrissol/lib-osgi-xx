/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_BUNDLEINSTALLERSERVICE_HH
#define OSGI_BUNDLEINSTALLERSERVICE_HH

#include "OSGi.hh"

/*! @file OSGi/BundleInstallerService.hh
    @brief Class OSGi::BundleInstallerService header.
    @author @ref Guillaume_Terrissol
    @date 9th January 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include "Context.hh"
#include "Service.hh"

namespace OSGi
{
//------------------------------------------------------------------------------
//                               Bundle Installer
//------------------------------------------------------------------------------

    /*! @brief Bundle installer Service.
        @version 0.8

        This service gives a way to install bundles at any time.@n
        It is registered under the name @ref OSGi_Core_Installer.
     */
    class OSGI_EXPORT BundleInstallerService : public Service
    {
    public:
        //! @name Pointers
        //@{
        using Ptr = std::shared_ptr<BundleInstallerService>;                //!< Pointer to service.
        //@}
        //! @name Constructor & destructor
        //@{
explicit            BundleInstallerService(Context* pContext);              //!< Constructor.
virtual             ~BundleInstallerService();                              //!< Destructor.
        //@}
        //! @name Interface
        //@{
 static std::string staticName();                                           //!< This type (BundleInstallerService) name.

        Bundle::Ptr installBundle(const std::string& pPath) const;          //!< New bundle installation.
        Bundle::Ptr replaceBundle(const std::string& pSymbolicName,
                                  const std::string& pPath) const;          //!< Bundle update or replacement.
        //@}
        //! @name Implementation
        //@{
    protected:

virtual bool        isType(const std::string& pTypeName) const override;    //!< @copybrief Service::isType()


    private:

virtual std::string typeName() const override;                              //!< @copybrief Service::typeName()
        //@}
        OSGI_PIMPL()
    };
}

#endif  // OSGI_BUNDLEINSTALLERSERVICE_HH
