# libOSGi++ 

libOSGi++ is a lightweight (and partial) implementation of [OSGi](http://www.osgi.org/Main/HomePage), written in C++11.

It's a small framework for creating plugins, with versioning, dependency checking, and packaging of extra data.

Available platforms : Linux (should work fine on BSD), Windows (with MinGW/msys).

## Downloads

You can retrieve stable releases [there](https://bitbucket.org/gterrissol/lib-osgi-xx/downloads).

Or clone the sources with:
```
hg clone https://<user_name>@bitbucket.org/gterrissol/lib-osgi-xx
```

## Example

At least 3 files are required :

* a source file, in order to build a shared library,
* a **bundlespec** file, that will define the bundle contents,
* a **CMakeLists.txt** file, to setup the build.

### C++ source

```cxx
#include <ostream>

#include <OSGi/Activator.hh>
#include <OSGi/Context.hh>

namespace N
{
    class Activator : public OSGi::Activator
    {
    public:
                Activator() = default;
virtual         ~Activator() = default;
    private:
        void    doStart(OSGi::OSGi::Context* pContext) override;
        void    doStop(OSGi::OSGi::Context* pContext) override;
    };

    void Activator::doStart(OSGi::OSGi::Context* pContext)
    {
        pContext->out() << "Starting N::Activator" << std::endl;
    }

    void Activator::doStop(OSGi::OSGi::Context* pContext)
    {
        pContext->out() << "Stopping N::Activator" << std::endl;
    }
}

OSGI_REGISTER_ACTIVATORS()
    OSGI_DECLARE_ACTIVATOR(N::Activator)
OSGI_END_REGISTER_ACTIVATORS()
```

An activator is required for each bundle (putting it into a namespace is optional).

### bundlespec

```xml
<?xml version='1.0'?>
<bundlespec version="1.0">
    <manifest>
        <name>Deployment bundle</name>
        <symbolicName>fr.osgi.deployment</symbolicName>
        <version>2.1.0</version>
        <vendor>Old School Team</vendor>
        <copyright>GNU LGPL v2.1 2009-2015, OldSchoolGame</copyright>
        <activator>
            <class>N::Activator</class>
        </activator>
    </manifest>
    <code>
        fr.osgi.deployment.so,
        fr.osgi.deployment.dll
    </code>
    <files>
        ...
    </files>
</bundlespec>

```

**NB:** Some files, like properties, are bundled implicitly.

### CMakeLists.txt

```cmake
find_package(OSGi 1.0.0 EXACT REQUIRED COMPONENTS dev PATHS <path_to_OSGi_install_dir>/lib/CMake NO_DEFAULT_PATH)

add_bundle(fr.osgi.deployment SOURCES fr.osgi.deployment.cc VERSION 2.1.0)
```

The bundle version (here, **2.1.0**) shall be the one in the bundlespec file.
