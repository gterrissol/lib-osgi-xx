configure_file(${CMAKE_CURRENT_SOURCE_DIR}/osgi.dox.in ${CMAKE_CURRENT_BINARY_DIR}/osgi.dox)

find_package(Doxygen)
if(${DOXYGEN_FOUND})
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc)
    add_custom_target(
        doc
        WORKING_DIRECTORY ${OSGi_BINARY_DIR}/
        COMMENT "Doxygen'ing"
        COMMAND doxygen ${CMAKE_CURRENT_BINARY_DIR}/osgi.dox
    )
    install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/doc DESTINATION share/OSGi)
endif()
