/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#ifndef OSGI_VERSION_HH
#define OSGI_VERSION_HH

#include "OSGi.hh"

/*! @file src/OSGi/Version.hh
    @brief Class OSGi::Version header.
    @author @ref Guillaume_Terrissol
    @date 12th February 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

#include <string>

namespace OSGi
{
//------------------------------------------------------------------------------
//                                Bundle Version
//------------------------------------------------------------------------------

    /// @cond DEVELOPMENT

    /*! @brief Bundle version.
        @version 0.7
        @internal
     */
    class Version
    {
    public:
        //! @name Constructors & destructor
        //@{
                    Version();                                                  //!< Default constructor.
        explicit    Version(const std::string& pVersion);                       //!< [Nominal] Constructor.
                    ~Version();                                                 //!< Destructor.
        //@}
        //! @name Pieces of informations
        //@{
        bool        isEmpty() const;                                            //!< Empty (undefined) version.
        bool        isLabel() const;                                            //!< Unique version ?
        bool        isRange() const;                                            //!< Range version ?
        bool        contains(const Version& pOther) const;                      //!< Another version inclusion ?
        std::string toString() const;                                           //!< Conversion to string.
        //@}
        //! @name "External" pieces of informations
        //@{
 static bool        isLabel(const std::string& pVersion);                       //!< Unique version ?
 static bool        isRange(const std::string& pVersion);                       //!< Range version 
        //@}

    private:

 static size_t      readVersion(const std::string& pVersion, int pFigures[3]);  //!< Version numbers reading.
        //@name Bound management
        //@{
        /*! @brief Bound.
            @version 0.5
            @internal
         */
        struct Bound
        {
                    Bound(uint8_t pMaj, uint8_t pMin, uint8_t pRev, bool pInc); //!< Constructor.
            Bound&  operator=(int pFigures[3]);                                 //!< Assignment operator.

            uint8_t mMaj;                                                       //!< Major version number.
            uint8_t mMin;                                                       //!< Minor version number.
            uint8_t mRev;                                                       //!< Revision number.
            bool    mInc;                                                       //!< Included bound ?
        };


        friend bool    operator<(const Bound& pL, const Bound& pR);             //!< Strict less-than operator.
        friend bool    operator<=(const Bound& pL, const Bound& pR);            //!< Less-than operator.
        friend bool    operator==(const Bound& pL, const Bound& pR);            //!< Equality operator.

        Bound  mMin;                                                            //!< Lower bound.
        Bound  mMax;                                                            //!< Upper bound.
        //@}
    };

    /// @endcond
}

#endif  // OSGI_VERSION_HH
