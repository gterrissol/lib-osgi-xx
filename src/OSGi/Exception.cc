/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#include "Exception.hh"

/*! @file src/OSGi/Exception.cc
    @brief Class OSGi::Exception (non-inline) methods.
    @author @ref Guillaume_Terrissol
    @date 13th December 2009 - 13th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
//------------------------------------------------------------------------------
//                             Base Exception Class
//------------------------------------------------------------------------------

    /*! @param pMsg Reason that initiated this exception
     */
    Exception::Exception(std::string pMsg)
        : mMessage{pMsg}
    { }


    /*! Defaulted.
     */
    Exception::~Exception() noexcept = default;


    /*! @return The message this exception was built with
     */
    const char* Exception::what() const noexcept
    {
        return mMessage.c_str();
    }


//------------------------------------------------------------------------------
//                                 I/O Exception
//------------------------------------------------------------------------------

    /*! @copydetails Exception::Exception(std::string pMsg)
     */
    IOError::IOError(std::string pMsg)
        : Exception{pMsg}
    { }


    /*! Defaulted.
     */
    IOError::~IOError() noexcept = default;


//------------------------------------------------------------------------------
//                                Logic Exception
//------------------------------------------------------------------------------

    /*! @copydetails Exception::Exception(std::string pMsg)
     */
    LogicError::LogicError(std::string pMsg)
        : Exception{pMsg}
    { }


    /*! Defaulted.
     */
    LogicError::~LogicError() noexcept = default;


//------------------------------------------------------------------------------
//                               Bundle Exception
//------------------------------------------------------------------------------

    /*! @copydetails Exception::Exception(std::string pMsg)
     */
    BundleError::BundleError(std::string pMsg)
        : Exception{pMsg}
    { }


    /*! Defaulted.
     */
    BundleError::~BundleError() noexcept = default;
}
