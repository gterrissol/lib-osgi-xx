/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#include "Activator.hh"

/*! @file src/OSGi/Activator.cc
    @brief Class OSGi::Activator (non-inline) methods.
    @author @ref Guillaume_Terrissol
    @date 13th December 2009 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
    /*! Defaulted.
     */
    Activator::Activator() = default;


    /*! Pure and defined (even defaulted, yes, it's possible).
     */
    Activator::~Activator() = default;


    /*! Starts the bundle.
        @param pContext Execution context
        @sa doStart(Context* pContext)
     */
    void Activator::start(Context* pContext)
    {
        doStart(pContext);
    }


    /*! Stops the bundle.
        @param pContext Execution context
        @sa doStart(Context* pContext)
     */
    void Activator::stop(Context* pContext)
    {
        doStop(pContext);
    }


    /*! @fn void Activator::doStart(Context* pContext)
        This method is called during a bundle OSGi::kStarting stage, once every
        dependency has been resolved.

        Typically, this method is used for :
        - services registering,
        - events connection,
        - environment setup, so that the bundle can do its job.

        @param pContext Execution context; it remains valid for the whole bundle
        lifecycle (until stop() returns), and so may be kept for further access
     */

    /*! @fn void Activator::doStop(Context* pContext)
        This method is called during a bundle OSGi::kStopping stage.

        Typically, this method is used for :
        - services unregistering,
        - events disconnection,
        - environment cleanup.

        @param pContext Execution context (the one start() was previously called
        with)
      */

    /*! @page OSGi_Create_Activator_Page Activator definition
        When starting a bundle, an activator is retrieved from a library, and
        its start() method is executed. The bundlespec file indicates which
        activator to use, from which library, among the ones defined thanks to
        a bunch of macros.@n
        A typical use case is as follow :
        @code
OSGI_REGISTER_ACTIVATORS()
OSGI_DECLARE_ACTIVATOR(NS::MyActivator)
OSGI_DECLARE_ACTIVATOR(NS::MyExtraActivator)
OSGI_END_REGISTER_ACTIVATORS()
        @endcode
        Where @b NS::MyActivator and @b NS::MyExtraActivator are made
        available : they may be picked through the bundlespec file, using their
        fully qualified name (see @ref OSGi_Bundlespec_Page).
     */
}
