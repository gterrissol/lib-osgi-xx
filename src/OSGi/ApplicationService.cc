/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#include "ApplicationService.hh"

/*! @file src/OSGi/ApplicationService.cc
    @brief Class OSGi::ApplicationService (non-inline) methods.
    @author @ref Guillaume_Terrissol
    @date 1st September 2011 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
//------------------------------------------------------------------------------
//                Application Service : Constructor & Destructor
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    ApplicationService::ApplicationService() = default;


    /*! Defaulted.
     */
    ApplicationService::~ApplicationService() = default;


//------------------------------------------------------------------------------
//                        Application Service : Interface
//------------------------------------------------------------------------------

    /*! @return The ApplicationService type name
     */
    std::string ApplicationService::staticName()
    {
        return typeid(ApplicationService).name();
    }


    /*! @param pArgc    Application argument count
        @param pArgv    Application argument list
        @param pContext Execution context
        @return Status code (0 on success)
     */
    int ApplicationService::main(int pArgc, char* pArgv[], Context* pContext)
    {
        Args    lArgs;

        for(int lA = 0; lA < pArgc; ++lA)
        {
            lArgs.emplace_back(*pArgv++);
        }

        return process(lArgs, pContext);
    }


    /*! @copydetails ApplicationService::process(const Args& pArgs, OSGi::Context* pContext)
     */
    int ApplicationService::main(const Args& pArgs, Context* pContext)
    {
        return process(pArgs, pContext);
    }


//------------------------------------------------------------------------------
//                     Application Service : Implementation
//------------------------------------------------------------------------------

    /*! @copydetails Service::isA(const std::string& pTypeName) const
     */
    bool ApplicationService::isType(const std::string& pTypeName) const
    {
        return (staticName() == pTypeName) || Service::isType(pTypeName);
    }


    /*! @copydetails Service::typeName() const
     */
    std::string ApplicationService::typeName() const
    {
        return staticName();
    }


     /*! @fn int ApplicationService::process(const Args& pArgs, Context* pContext)
        @param pArgs Application arguments
        @param pContext Bundle manager context
        @return Status code (0 on success)
     */
}
