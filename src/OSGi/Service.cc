/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#include "Service.hh"

/*! @file src/OSGi/Service.cc
    @brief Class OSGi::Service (non-inline) methods.
    @author @ref Guillaume_Terrissol
    @date 27th December 2009 - 19th January 2014
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    Service::Service() = default;


    /*! Defaulted.
     */
    Service::~Service() = default;


    /*! @return The class name
        @note Such a static member function should be defined in every service class
        @note The returned value is implementation-dependent, and thus, not
              portable across compilers
     */
    std::string Service::staticName()
    {
        return typeid(Service).name();
    }


    /*! @copydetails Service::typeName() const
     */
    std::string Service::dynamicName() const
    {
        return typeName();
    }


    /*! @param pTypeName Type name to compare to the instance real type
        @retval true if the instance is of type @p pTypeName
        @retval false otherwise
     */
    bool Service::isA(const std::string& pTypeName) const
    {
        return (typeName() == pTypeName);
    }


    /*! A standard implementation is written as :@code
    std::string MyService::isType(const std::string& pTypeName) const
    {
        return (staticName() == pTypeName) || ParentClass::isType(pTypeName);
    }@endcode
        @copydetails Service::isA(const std::string& pTypeName) const
     */
    bool Service::isType(const std::string& pTypeName) const
    {
        return (staticName() == pTypeName);
    }


    /*! @return The instance real type name
     */
    std::string Service::typeName() const
    {
        return staticName();
    }

    
//------------------------------------------------------------------------------
//                           Additional Documentation
//------------------------------------------------------------------------------

    /*! @page OSGi_Default_Services_Page Default services
        The following Service<small>s</small> are available :
        @section OSGi_Core_Installer osgi.core.installer
        BundleInstallerService : this service allows to dynamicaly install @ref Bundle "bundles".

        @section OSGi_Core_XP osgi.core.xp
        ExtensionPointService : this service allows to manage @ref ExtensionPoint "extension points", which are, with
        @ref Service "services", a common way to extend an application.
     */
}
