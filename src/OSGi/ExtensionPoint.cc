/*  This program is free software; you can redistribute it and/or modify it
    under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation; either version 2.1 of the License, or (at
    your option) any later version.
    For more details, see the GNU Lesser General Public License (www.fsf.org
    or the COPYING file somewhere in the package)
 */

#include "ExtensionPoint.hh"

/*! @file src/OSGi/ExtensionPoint.cc
    @brief Class OSGi::ExtensionPoint (non-inline) methods.
    @author @ref Guillaume_Terrissol
    @date 26th January 2010 - 26th May 2013
    @note This file is distributed under the LGPL license.
    Refer to the file COPYING (or http://www.fsf.org) for more information.
 */

namespace OSGi
{
//------------------------------------------------------------------------------
//                           Constructor & Destructor
//------------------------------------------------------------------------------

    /*! Defaulted.
     */
    ExtensionPoint::ExtensionPoint() = default;


    /*! Defaulted.
     */
    ExtensionPoint::~ExtensionPoint() = default;


//------------------------------------------------------------------------------
//                                   Interface
//------------------------------------------------------------------------------

    /*! @param pBundle    This Bundle brings this extension point logic or data 
        @param pExtension XML data brought by @p pBundle in order to extend the
               bundle owning this instance
        @note @p pBundle is a constant pointer to prevent the extension point to
        modify the bundle
     */
    void ExtensionPoint::handleExtension(Bundle::CPtr pBundle, const XmlElement* pExtension)
    {
        doHandleExtension(pBundle, pExtension);
    }


    /*! @param pBundle    This Bundle brought this extension point logic or data 
        @param pExtension XML data brought by @p pBundle in order to extend the
               bundle owning this instance
        @note @p pBundle is a constant pointer to prevent the extension point to
        modify the bundle
     */
    void ExtensionPoint::removeExtension(Bundle::CPtr pBundle, const XmlElement* pExtension)
    {
        doRemoveExtension(pBundle, pExtension);
    }


//------------------------------------------------------------------------------
//                                Implementation
//------------------------------------------------------------------------------

    /*! @fn ExtensionPoint::doHandleExtension(Bundle::CPtr, const XmlElement*)
        Unlike doRemoveExtension, there is no default implementation for this
        method, because the real work shall happen here.
        @copydetails handleExtension(Bundle::CPtr, const XmlElement*)
     */


    /*! Default implementation does nothing.
        @copydetails removeExtension(Bundle::CPtr, const XmlElement*)
     */
    void ExtensionPoint::doRemoveExtension(Bundle::CPtr, const XmlElement*) { }


//------------------------------------------------------------------------------
//                           Additionnal Documentation
//------------------------------------------------------------------------------

    /*! @class ExtensionPoint
        An extension point alllows a bundle extending its functionality in any
        way. Every customization is an @ref OSGi_Extension_Page "extension" in
        itself.@n
        A bundle may register one (or more) extension points to the dedicated
        @ref OSGi_Core_XP "service" thanks to the
        ExtensionPointService::checkIn() method.@n
        In order to "feed" an extension point, a bundle must embed an
        @ref OSGi_Extensions_xml_Page file.
        @note When a bundle providing an extension is stopped, removeExtension()
        is called automatically
     */


     /*! @page OSGi_Extension_Page Extension
        An extension is a piece of logic or data, brought by a bundle, to a
        given ExtensionPoint (which knows how to handle this logic or data, see
        ExtensionPoint::doHandleExtension(Bundle::CPtr, const XmlElement*)). @n
        Extensions are defined in the @ref OSGi_Extensions_xml_Page file.
      */
}
